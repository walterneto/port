import java.util.Scanner;

public class Main {

    public static void main(String args[]){





//       1 - Faça um algoritmo que leia a idade de uma pessoa expressa em anos, meses e dias e
//           mostre-a expressa em dias. Leve em consideração o ano com 365 dias e o mês com 30.
//           (Ex: 3 anos, 2 meses e 15 dias = 1170 dias.)

        int anos, meses, dias;

        System.out.println("Digite a idade em anos, meses e dias");
        Scanner scn = new Scanner(System.in);

anos = scn.nextInt();
meses = scn.nextInt();
dias = scn.nextInt();

dias = anos * 365 + meses * 30 + dias;
        System.out.println("A pessoa tem " + dias + " dias de vida");

//        --------------------------------------------------------------------------------------
//
//       2 -  Fazer um programa que imprima a média aritmética dos números 8,9 e 7. A média dos
//            números 4, 5 e 6. A soma das duas médias. A média das médias.


   double media1 = (8+9+7)/3;
   double media2 = (4+5+6)/3;
   double media3 = media1 + media2;
   double soma = media3/2;

        System.out.println("A média aritmética dos numeros 8, 9, 7 é " + media1);
        System.out.println("A média aritmética dos numeros 4, 5, 6 é " + media2);
        System.out.println("A soma das duas médias aritméticas é " + media3);
        System.out.println("A média aritmética das médias é " + soma);

//        --------------------------------------------------------------------------------------
//
//        3 - Informar um saldo e imprimir o saldo com reajuste de 1%.

       int saldo = 500;
       saldo = saldo + (saldo*1)/100;

       System.out.println("O saldo é de R$ " + saldo);
       System.out.println("O saldo com reajuste de 1% é de R$ " + saldo);

//        --------------------------------------------------------------------------------------
//
//        4 - Escrever um algoritmo que lê:
//        - a porcentagem do IPI a ser acrescido no valor das peças
//        - o código da peça 1, valor unitário da peça 1, quantidade de peças 1
//                - o código da peça 2, valor unitário da peça 2, quantidade de peças 2
//        O algoritmo deve calcular o valor total a ser pago e apresentar o resultado.
//        Fórmula : (valor1*quant1 + valor2*quant2)*(IPI/100 + 1)

        int cod1, valor1, qnt1, cod2, valor2, qnt2;
        double IPI;

        Scanner scn = new Scanner(System.in);

        System.out.println("Digite o IPI");
        IPI = scn.nextInt();

        System.out.println("Digite a peça 1 : o codigo, valor unitário e quantidade de peças");
        cod1 = scn.nextInt();
        valor1 = scn.nextInt();
        qnt1 = scn.nextInt();

        System.out.println("Digite a peça 2 : o codigo, valor unitário e quantidade de peças");
        cod2 = scn.nextInt();
        valor2 = scn.nextInt();
        qnt2 = scn.nextInt();

        System.out.println("valor total é :" + (valor1*qnt1 + valor2*qnt2)*(IPI/100 + 1));

//        --------------------------------------------------------------------------------------
//
//        5 - Crie um algoritmo que leia o valor do salário mínimo e o valor do salário de um usuário,
//        calcule a quantidade de salários mínimos esse usuário ganha e imprima o resultado.
//        (1SM=R$788,00)

        int sm = 788, su;

        Scanner scn = new Scanner(System.in);

        System.out.println("Digite seu salário");
        su = scn.nextInt();

        System.out.println("Você ganha " + su/sm + " Salários minimos");

//         --------------------------------------------------------------------------------------
//
//        6 - Desenvolva um algoritmo em Java que leia um número inteiro e imprima o seu
//        antecessor e seu sucessor

         int num;
        Scanner scn = new Scanner(System.in);
        System.out.println("Digite o numero");
        num = scn.nextInt();

        System.out.println("O Antecessor é " + (num-1) + " e Sucessor é " + (num+1));



















    }


}
