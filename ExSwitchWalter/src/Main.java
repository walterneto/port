import java.util.Scanner;

public class Main {

    public static void main(String args[]) {



        Scanner scn = new Scanner(System.in);

        System.out.println("Digite Dois Numeros Reais");
        double num1 = scn.nextDouble();
        double num2 = scn.nextDouble();
        double resultado = 0;

        System.out.println("Digite uma das operações [+ , - , / ou *]");
        String operacao = scn.next();

        switch(operacao)
        {
            case "+": { resultado = num1+num2; break; }
            case "-": { resultado = num1-num2; break; }
            case "/": { resultado = num1/num2; break; }
            case "*": { resultado = num1*num2; break; }
        }
 System.out.println(String.format("Resultado: %,.2f + %,.2f = %,.2f", num1, num2, resultado));




    }


}
